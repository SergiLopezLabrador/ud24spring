package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UD24SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(UD24SpringApplication.class, args);
	}

}
