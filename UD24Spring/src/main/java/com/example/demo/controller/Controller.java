package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.trabajador;
import com.example.demo.service.Impl;

@RestController
//Aquí creamos la ruta raíz del Spring
@RequestMapping("/api")
public class Controller {
	
	@Autowired
	Impl Impl;
	
	//Aquí cre
	@GetMapping("/Trabajador")
	public List<trabajador> listarTrabajador(){
		return Impl.listarTrabajador();
	}
	//Aquí crearemos un ruta que permite buscar por trabajo (en el postman)
	@GetMapping("/Trabajador/trabajo/{trabajo}")
	public List<trabajador> buscarTrabajo(@PathVariable(name = "trabajo") String trabajo){
		return Impl.buscarTrabajo(trabajo);
	}
	//Aquí creamos la ruta que irá posteriormente a la de la raíz
	@PostMapping("/Trabajador")
	public trabajador salvarCliente(@RequestBody trabajador trabajador) {
		
		return Impl.guardarTrabajador(trabajador);
	}

	//Aquí podemos especificar la id del trabajador que queremos buscar
	@GetMapping("/Trabajador/{id}")
	public trabajador trabajadorXId(@PathVariable(name = "id") Long id) {
		
		trabajador trabajador_xid = new trabajador();
		
		trabajador_xid = Impl.trabajadorXID(id);
		
		System.out.println("Trabajador por ID: " + trabajador_xid);
		
		return trabajador_xid;
	}
	//Con este método podemos eliminar un trabajador en específico
	@DeleteMapping("/Trabajador/{id}")
	public void eliminarTrabajador(@PathVariable(name="id")Long id) {
		Impl.eliminarTrabajador(id);
	}
	//Con este método actualizamos el trabajador con la id específica que queramos
	@PutMapping("/Trabajador/{id}")
	public trabajador actualizarTrabajador(@PathVariable(name="id")Long id,@RequestBody trabajador trabajador) {
		
		trabajador trabajadorSeleccionado = new trabajador();
		trabajador trabajadorActualizado = new trabajador();
		
		trabajadorSeleccionado = Impl.trabajadorXID(id);
		
		trabajadorSeleccionado.setNombre(trabajador.getNombre());
		trabajadorSeleccionado.setTrabajo(trabajador.getTrabajo());
		trabajadorSeleccionado.setSalario(com.example.demo.dto.trabajador.asignarSalario(trabajador.getTrabajo()));
		
		trabajadorActualizado = Impl.actualizarTrabajador(trabajadorSeleccionado);
		
		System.out.println("El que se acaba de actualizar es: " + trabajadorActualizado);
		
		return trabajadorActualizado;
		
	}
	

	
	
}
