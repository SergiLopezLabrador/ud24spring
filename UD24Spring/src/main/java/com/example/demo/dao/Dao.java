package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.trabajador;

//Aquí enlazamos el dao con la base de datos
public interface Dao extends JpaRepository<trabajador, Long> {

	public List<trabajador> findByTrabajo(String trabajo);
	
}

