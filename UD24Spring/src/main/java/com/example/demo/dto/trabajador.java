package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.*;

//Aquí creo el constructor trabajador, que usaremos a la larga del programa 
@Entity
@Table(name="trabajador")
public class trabajador {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (name = "nombre")
	private String nombre;
	
	@Column (name = "trabajo")
	private String trabajo;
	
	@Column (name = "salario")
	private int salario;
	
	public trabajador() {
		
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param trabajo
	 * @param salario
	 */
	//Aquí creamos el constructor por defecto y le asignamos el valor null al salario
	public trabajador(Long id, String nombre, String apellido, String trabajo) {

		this.id = id;
		this.nombre = nombre;
		this.trabajo = trabajo;
		this.salario = 0;
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param trabajo
	 * @param salario
	 */
	//Aquí creamos el constructor con todos los datos por defecto
	public trabajador(Long id, String nombre, String apellido, String trabajo, int salario) {

		this.id = id;
		this.nombre = nombre;
		this.trabajo = trabajo;
		this.salario = salario;
	}

	//------------------------------------- AQUI CREAMOS TOXDOS LOS GETTERS Y LOS SETTERS ------------------------
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTrabajo() {
		return trabajo;
	}

	public void setTrabajo(String trabajo) {
		this.trabajo = trabajo;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}
	
	//Aquí creo el método asignar Salario donde asigno un salario a cada trabajo existente
	public static int asignarSalario(String trabajo) {
		int salario = 0;
		
		if(trabajo.contentEquals("policia")) {
			salario = 45000;
		}else if(trabajo.contentEquals("profesor")) {
			salario = 60000;
		}else if(trabajo.contentEquals("administrativo")) {
			salario = 75000;
		}else if(trabajo.contentEquals("programador")) {
			salario = 30500;
		}else if(trabajo.contentEquals("soldador")) {
			salario = 60000;
		}else if(trabajo.contentEquals("transportista")) {
			salario = 70000;
		}
		
		return salario;
		
	}

	//Aquí creamos un toString para printar los datos por consola
	@Override
	public String toString() {
		return "trabajador [id=" + id + ", nombre=" + nombre + ", trabajo=" + trabajo
				+ ", salario=" + salario + "]";
	}
	

	
	
}
