package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.Dao;
import com.example.demo.dto.trabajador;

@Service
public class Impl {
	
	@Autowired
	Dao trabajadorDao;
	//Aqui creamos una lista de objetos que serán enviados al DAO y finalmente el dao enviará los datos a la base de datos
	public List<trabajador> listarTrabajador() {
		return trabajadorDao.findAll();
	}
	//Aqui creamos una lista de objetos que serán enviados al DAO y finalmente el dao 
	// enviará los datos a la base de datos donde podremos buscar el Trabajo indicado
	public List<trabajador> buscarTrabajo(String trabajo) {
		return trabajadorDao.findByTrabajo(trabajo);
	}
	//Aquí enviaremos la misma respuesta a la base de datos pero esta vez será una petición para guardar
	public trabajador guardarTrabajador(trabajador trabajador) {
		trabajador.setSalario(com.example.demo.dto.trabajador.asignarSalario(trabajador.getTrabajo()));
		return trabajadorDao.save(trabajador);
	}
	//Aquí crearemos el método que usaremos para enlazar el trabajador con la id del mismo
	public trabajador trabajadorXID(Long id) {
		return trabajadorDao.findById(id).get();
	}

	//Aquí creo el método que usaré para actualizar el trabajador indicado
	public trabajador actualizarTrabajador(trabajador trabajador) {
		
		return trabajadorDao.save(trabajador);
	}
	//Aquí eliminar el trabajador dependiendo de la id que le corresponda
	public void eliminarTrabajador(Long id) {
		
		trabajadorDao.deleteById(id);
		
	}
}
