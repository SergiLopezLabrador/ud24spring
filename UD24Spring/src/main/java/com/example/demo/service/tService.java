package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.trabajador;

public interface tService {
	
	//Aquí creamos una interface con todos los métodos y listaas como default
	
	public List<trabajador> listarTrabajador();
	
	public trabajador guardarTrabajador(trabajador trabajador);
	
	public trabajador trabajadorXId(Long id);
	
	public trabajador actualizarTrabajador(trabajador trabajador);
	
	public void eliminarTrabajador(Long id);
}
