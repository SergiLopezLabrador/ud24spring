DROP table IF EXISTS trabajador;

create table trabajador(
	id int auto_increment primary key,
	nombre varchar(250),
	trabajo varchar(250),
	salario integer
);

insert into trabajador (nombre, trabajo, salario)values('Antonio','policia',30001);